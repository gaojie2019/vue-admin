import path from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { autoComplete, Plugin as importToCDN } from 'vite-plugin-cdn-import'

// https://vitejs.dev/config/
export default defineConfig({
  base: './',
  build: {
    sourcemap: false,
    terserOptions: {
      compress: {
        //生产环境时移除console
        drop_console: true,
        drop_debugger: true,
      },
    }
  },
  plugins: [
    vue(),
    importToCDN({
      modules: [
        {
          name: 'vue',
          var: 'Vue',
          path: 'https://cdn.staticfile.org/vue/3.3.2/vue.global.prod.min.js'
        },
        {
          name: 'vuex',
          var: 'Vuex',
          path: 'https://cdn.staticfile.org/vuex/4.1.0/vuex.global.prod.min.js'
        },
        {
          name: 'vue-router',
          var: 'VueRouter',
          path: 'https://cdn.staticfile.org/vue-router/4.2.0/vue-router.global.prod.min.js'
        },
        {
          name: 'element-plus',
          var: 'ElementPlus',
          path: 'https://cdn.staticfile.org/element-plus/2.3.4/index.full.min.js',
          css: 'https://cdn.staticfile.org/element-plus/2.3.4/index.min.css',
        },
        {
          name: 'echarts',
          var: 'echarts',
          path: 'https://cdn.staticfile.org/echarts/5.4.2/echarts.min.js'
        },
        {
          name: '@element-plus/icons-vue',
          var: 'ElementPlusIconsVue',
          path: 'https://cdn.staticfile.org/element-plus-icons-vue/2.1.0/index.iife.min.js'
        },
        {
          name: 'axios',
          var: 'axios',
          path: 'https://cdn.staticfile.org/axios/1.4.0/axios.min.js'
        },
      ]
    })],
  server: {
    host: '0.0.0.0',
    port: 8888,
    open: true,
    proxy: {
      '/api': {
        target: 'http://localhost',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src')
    }
  }
})
