const state = () => ({
    device: 0,
    collapse: false,
    drawer: false,
    refresh: true,
    menu: [],
    user: {}
})

const mutations = {
    setMenu(state, data) {
        state.menu = data
    },
    setUser(state, data) {
        state.user = data
    },
    setCollapse(state, value) {
        state.collapse = value
    },
    setDrawer(state, value) {
        state.drawer = value
    },
    setDevice(state, value) {
        state.device = value
    },
    refreshRoute(state) {
        state.refresh = false
        setTimeout(() => {
            state.refresh = true
        }, 100)
    }
}

const actions = {
    setMenu: ({ commit }, data) => commit('setMenu', data),
    setUser: ({ commit }, data) => commit('setUser', data),
    setCollapse: ({ commit }, value) => commit('setCollapse', value),
    setDrawer: ({ commit }, value) => commit('setDrawer', value),
    setDevice: ({ commit }, value) => commit('setDevice', value),
    refreshRoute: ({ commit }) => commit('refreshRoute')
}

const getters = {

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
